console.log('page load - entered main.js for js-other api');

var sendButton = document.getElementById('send-button');
sendButton.onmouseup = getFormInfo;

function getFormInfo(){
  console.log('entered getFormInfo!');
  var server_address = document.getElementById("select-server-address").value;
  console.log('Server Address: ' + server_address);
  var port_number = document.getElementById("input-port-number").value;
  console.log('Port Number: ' + port_number);
  var get_radio = document.getElementById("radio-get").checked;
  var put_radio = document.getElementById("radio-put").checked;
  var post_radio = document.getElementById("radio-post").checked;
  var delete_radio = document.getElementById("radio-delete").checked;
  var http_type = ''
  if (get_radio) {
    http_type = 'GET'
  } else if (put_radio) {
    http_type = 'PUT'
  } else if (post_radio) {
    http_type = 'POST'
  } else if (delete_radio) {
    http_type = 'DELETE'
  }
  console.log('HTTP Type: ' + http_type);
  var key = document.getElementById("input-key").value;
  console.log('Key: ' + key);
  var use_key = document.getElementById("checkbox-use-key").checked;
  console.log('Use Key: ' + use_key);
  var use_message = document.getElementById("checkbox-use-message").checked;
  console.log('Use Message: ' + use_message);
  var message = document.getElementById("text-message-body").value;
  console.log('Message: ' + message);


  makeNetworkCall(server_address, port_number, http_type, key, use_key, message, use_message);
} // end of get form info

function makeNetworkCall(server_address, port_number, http_type, key, use_key, message, use_message){
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
  var url = 'http://' + server_address + ':' + port_number + '/movies/';
  
  if (use_key) {
    url += key;
  }
  xhr.open(http_type, url, true); // 2 - associates request attributes with xhr

  // set up onload
  xhr.onload = function(e) { // triggered when response is received
      // must be written before send
      console.log(xhr.responseText);
      // do something
      updatePageWithResponse(xhr.responseText);
  }

  // set up onerror
  xhr.onerror = function(e) { // triggered when error response is received and must be before send
      console.error(xhr.statusText);
  }

  // actually make the network call
  var body = null;

  if (use_message) {
    body = message;
  }

  xhr.send(body) // last step - this actually makes the request

} // end of make nw call

function updatePageWithResponse(response_text){
    // update a label
    var label = document.getElementById("answer-label");

    label.innerHTML =  response_text;
    
} // end of updateAgeWithResponse
